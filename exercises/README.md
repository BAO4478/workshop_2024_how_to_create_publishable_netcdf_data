## Examples and Data Sets
The following ASCII, binary or netCDF data sets were used to show the conversion to CF compliant netCDF files:

### DWD stations
We have atmospheric measurement stations from the German Meteorological Service (DWD) in ASCII:
/data/icdc/atmosphere/dwd_stations/DATA/. The original (more complicated) data format can be accessed here: https://opendata.dwd.de/climate_environment/CDC/observations_germany/climate/daily/kl/
- **DWD station convert** converts a single station to a netCDF file
- **DWD station convert time origin** does the same, but shows how to control the time origin and how to set the time dimension to UNLIMITED


### Radolan
You can also convert a binary data set: 
https://opendata.dwd.de/climate_environment/CDC/grids_germany/5_minutes/radolan/reproc/2017_002/bin/2023/
- DWD_binary_to_netCDF shows how you can read in radolan binary data by means of xarray


### Sea ice volume
You can find a sea ice volume data set here: /data/icdc_main/DATA/ice_and_snow/esa_cci_sicci/, the file is called ESA-SICCI2_ICDC__SeaIceVolumeTimeseries__south_all_v2.0_fv0.01.txt
- **sea ice area** converts a CSV to a netCDF file

###  Channel Islands National Park's Kelp Forest Monitoring Program
This dataset has netCDF files from the Channel Islands National Park's Kelp Forest Monitoring Program containing subtidal temperature data taken at permanent monitoring sites: 
https://coastwatch.pfeg.noaa.gov/erddap/files/erdCinpKfmT/
- **multiple time series different time values** shows an example how to read in 2 of these data sets and create a netCDF file by means of CF Conventions' template H.2.2

### DSG examples for Profiles, Time Series, Trajectories
The example notebooks starting with DSG_ show all templates of the DSG CF Appendix.
- DSG_profile
- DSG_timeSeries
- DSG_trajectory

### Sea Surface Temaperature
The example of our netCDF and CF Introduction can be downloaded here: https://www.unidata.ucar.edu/software/netcdf/examples/files.html
