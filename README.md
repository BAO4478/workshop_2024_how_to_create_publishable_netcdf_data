# Workshop Material: How to create publishable netcdf data

The workshop deals with creating publishable netCDF (Network Common Data Format) data from the Earth System Sciences (ESS). Data Centers have certain demands regarding the content and format of ESS data - we will focus in this workshop on creating CF (Climate and Forecast) compliant netCDF files.

We encourage the target group - ESS doctoral candidates, master students and scientists - to bring their own data, but we will also provide data sets that can be used during the workshop. Prerequisites are programming skills in Python.

The workshop participants will work on their data after a short introduction to the netCDF data format, the CF conventions and how to create CF compliant netCDF files in Python. In the end they will show their results in short presentations in order to discuss the problems that occured and their solutions, as well as getting an insight in each others scientific work.

The order of the material is the following:
1. PART1: Introduction to the netCDF format
2. PART2: Introduction to the CF conventions
3. PART3: Using XARRAY to create CF compliant netCDF files
4. PART4: IT environment and exercises
5. PART5: Manipulating netCDF files by means of NCO (optional)

Under DATA/ you can find some of the used datasets, under exercises/ the solutions to the exercises

After the workshop, the participants will be able to answer the following questions:
* What is the netCDF data format?
* What are the CF conventions?
* What are essential metadata in the netCDF header?
* How do I create CF compliant netCDF files?
* How do I deal with special formats (single insitu measurements, ship observations, profiles, etc.) in the netCDF format?

## Authors and acknowledgment
Kemeng Liu, Stefan Kern, Remon Sadikni 

Universität Hamburg, NFDI4Earth

## References / Sources
We used data of 
- UNIDATA: https://www.unidata.ucar.edu/software/netcdf/examples/files.html (here we used the TOS sea surface temperature file from 2001-2002)
- netCDF files from the Channel Islands National Park's Kelp Forest Monitoring Program containing subtidal temperature data taken at permanent monitoring sites: 
https://coastwatch.pfeg.noaa.gov/erddap/files/erdCinpKfmT/
- DWD data:
    - Radolan: https://opendata.dwd.de/climate_environment/CDC/grids_germany/5_minutes/radolan/reproc/2017_002/bin/2023/
    - DWD measurement stations: https://opendata.dwd.de/climate_environment/CDC/observations_germany/climate/daily/kl/
- RDMkit: We used the picture of the data life cycle from https://rdmkit.elixir-europe.org/data_life_cycle

